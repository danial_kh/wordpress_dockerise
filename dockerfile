# syntax=docker/dockerfile:1
FROM ubuntu
RUN apt-get update && apt-get install nginx -y && apt-get install -y wget
RUN apt -y install software-properties-common && add-apt-repository ppa:ondrej/php
RUN apt-get install php8.0 php8.0-cli php8.0-fpm php8.0-mysql php8.0-opcache php8.0-mbstring php8.0-xml php8.0-gd php8.0-curl php8.0-imagick php8.0-zip -y
RUN apt-get install php7.4 php7.4-cli php7.4-fpm php7.4-mysql php7.4-json php7.4-opcache php7.4-mbstring php7.4-xml php7.4-gd php7.4-curl php7.4-imagick php7.4-zip -y
RUN wget http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz
RUN tar xzf ioncube_loaders_lin_x86-64.tar.gz -C /usr/local
RUN rm -rf ioncube_loaders_lin_x86-64.tar.gz
workdir /var/www/

RUN mkdir web1
RUN chown -R www-data:www-data web1/
RUN find web1/ -type d -exec chmod 755 {} \; && find web1/ -type f -exec chmod 644 {} \;

RUN mkdir web2
RUN chown -R www-data:www-data web2/
RUN find web2/ -type d -exec chmod 755 {} \; && find web2/ -type f -exec chmod 644 {} \;

COPY ./start.sh /var/www/
COPY ./nginx/conf/wordpress.conf /etc/nginx/sites-available/
RUN ln -s /etc/nginx/sites-available/wordpress.conf /etc/nginx/sites-enabled/
RUN rm /etc/nginx/sites-enabled/default
#ENTRYPOINT ["tail", "-f", "/dev/null"]
CMD ["sh","/var/www/start.sh"]
