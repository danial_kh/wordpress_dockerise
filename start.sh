#!/bin/bash

service php7.4-fpm start
service php8.0-fpm start
service nginx start
tail -F /dev/null